import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class App extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        localizationsDelegates: <LocalizationsDelegate<dynamic>>[
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate
        ],
        supportedLocales: <Locale>[
          Locale.fromSubtags(languageCode: 'ru', countryCode: 'RU'),
          Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
        ],
        locale: Locale.fromSubtags(languageCode: 'ru', countryCode: 'RU'),
        initialRoute: '/',
    );
  }
}
